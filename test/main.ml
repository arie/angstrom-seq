(* This is taken from rdf-xml *)
let of_file file =
  let ic = open_in file in
  let rec read () =
    try
      let line = input_char ic in
      Seq.Cons (line, read)
    with
    | End_of_file ->
      close_in ic;
      Seq.Nil
    | e ->
      close_in_noerr ic;
      raise e
  in read

let parse_once p filename on_parse =
  let seq = of_file filename in
  let unconsumed, result' = Angstrom_seq.parse p seq in
  match result' with
  | Ok result ->
    let () = on_parse result in
    let () = Fmt.pr "num unconsumed chars: %d \n" unconsumed.len in
    Ok result
  | Error s ->
    let () = Fmt.pr "parse error: %s" s in
    Error s

let parse_many p filename on_parse =
  let seq = of_file filename in
  let unconsumed, result' = Angstrom_seq.parse_many p on_parse seq in
  match result' with
  | Ok () ->
    let () = Fmt.pr "Finished parsing stream \n" in
    let () = Fmt.pr "num unconsumed chars: %d \n" unconsumed.len in
    Ok ()
  | Error s ->
    let () = Fmt.pr "parse error: %s" s in
    Error s

let test_case_once p file on_parse = Alcotest.test_case "Once" `Quick (fun () ->
    match parse_once p file on_parse with
    | Ok (_result) -> ()
    | Error s -> Alcotest.fail @@ Fmt.str "Parsing failed: %s" s)

let test_case_many p file on_parse = Alcotest.test_case "Many" `Quick (fun () ->
    match parse_many p file on_parse with
    | Ok () -> ()
    | Error s -> Alcotest.fail @@ Fmt.str "Parsing failed: %s" s)

let on_parse_message m = Fmt.pr "\n Message parsed: %a \n" Message.pp m
let on_parse_rec_type r = Fmt.pr "\n Rec_type parsed: %a \n" Rec_type.Pp.c r

let () =
  Alcotest.run ~verbose:true "Seq_parser" [
    "Message", [
      test_case_once Message.Parser.message "../examples/message.txt" on_parse_message ;
      test_case_many Message.Parser.message "../examples/message.txt" on_parse_message
    ];
    "Rec_type", [
      test_case_once Rec_type.Parser.c' "../examples/rec_type.txt" on_parse_rec_type ;
      test_case_many Rec_type.Parser.c' "../examples/rec_type.txt" on_parse_rec_type
    ]
  ]

