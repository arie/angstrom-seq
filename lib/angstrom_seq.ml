let handle_parse_result state =
  let open Angstrom.Buffered in
  match state_to_unconsumed state with
  | None    -> assert false
  | Some us -> us, state_to_result state

(** This version of buffered_state_loop loops for each character.
 * @not-tested, might be slow *)
let rec buffered_state_loop state seq =
  let open Angstrom.Buffered in
  match state with
  | Partial k ->
    begin match Seq.uncons seq with
      | None -> k `Eof
      | Some (head, tail) ->
          k (`String (String.make 1 head)) |> (fun state' ->
          buffered_state_loop state' tail)
    end
  | state -> state

let with_buffered_parse_state state seq =
  let open Angstrom.Buffered in
  begin match state with
    | Partial _ -> buffered_state_loop state seq
    | _         -> state
  end
  |> handle_parse_result

let parse p in_seq =
  let open Angstrom.Buffered in
  let state = buffered_state_loop (parse p) in_seq in
  handle_parse_result state

let parse_many p on_parse in_seq = parse Angstrom.(skip_many (p <* commit >>| on_parse)) in_seq
