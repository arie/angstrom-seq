(** @not-tested Provides more fine-grained control over what happens when parsing
    fails or succeeds compared to parse_many. *)
val with_buffered_parse_state :
            'a Angstrom.Buffered.state ->
            char Seq.t ->
            Angstrom.Buffered.unconsumed * ('a, string) result

(** This function accepts a parser and a sequence of chars. It tries to parse the first characters of the sequence, and returns the parsed object if it succeeds. If it does not succeed, it returns an Error. In both cases, it also returns the unconsumed part of the sequence, but now as Angstrom.Buffered.unconsumed. *)
val parse : 'a Angstrom.t ->
            char Seq.t ->
            Angstrom.Buffered.unconsumed * ('a, string) result

(** This function accepts an 'a parser, a function k which takes 'a as argument, and a sequence of chars. It tries to parse the first characters, and if it succeeds, it applies the function k to the result, and continues the same procedure with the tail of the sequence. If the parsing fails it returns an error. *)
val parse_many :'a Angstrom.t ->
            ('a -> 'b) ->
            char Seq.t ->
            Angstrom.Buffered.unconsumed * (unit, string) result

