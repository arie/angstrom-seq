(* this is a 'recursive' type; a can depend on b, and b on a *)
(* the idea is to parse things like: - "[[(-abcde-)]]" -> parse -> A (A (B (String abcde)))
  - "([([-hello-])])" -> parse -> B (A (B (A (String hello)))) *)

type a =
  A_a of a |
  A_b of b |
  A_s of string
and b =
  B_a of a |
  B_b of b |
  B_s of string

type c = C_a of a | C_b of b

type t = c

module Parser = struct
  open Angstrom

  (* this is a trick to define a parser for mutually recursive types.
     For types that depend only on itself, fix suffices to parse it
     (I think json is an example), but for 2 or more types, depending on
     each other, the following seems necessary *)
  (* the trick works as follows:
     - define function make_a, depending on b, as a fix of a
       You can use both a and b there, b because it's a variable, a
       because you're in the fix
     - define b as a fix of b, then use make_a to compute a
       You can use b, because fix, and use a = make_a b.
     - define a = make_a b, then you have both a and b *)
  (* the trick can be generalized to a b c ..., as many as you like,
     but the codesize increases a lot *)

  let delimiters c1 c2 p =
    char c1 *> p <* Angstrom.char c2

  let delimiters_string_not_greedy c1 c2 = delimiters c1 c2 @@
    take_while (fun d -> not @@ Char.equal c2 d)

  let dashed = delimiters_string_not_greedy '-' '-'
  let bracketed p = delimiters '[' ']' p
  let parenthesized p = delimiters '(' ')' p

  let make_a b = fix (fun a ->
    lift (fun s -> A_s s) dashed
    <|>
    lift (fun a -> A_a a) (bracketed a)
    <|>
    lift (fun b -> A_b b) (parenthesized b)
  )

  let b = fix (fun b ->
    let a = make_a b in
    lift (fun s -> B_s s) dashed
    <|>
    lift (fun b -> B_b b) (parenthesized b)
    <|>
    lift (fun a -> B_a a) (bracketed a)
  )

  let a = make_a b

  let c =
    (* the peek_char is not strictly necessary, but the output is a bit nicer *)
    peek_char >>= function
      | Some '[' -> lift (fun a -> C_a a) (bracketed a)
      | Some '(' -> lift (fun b -> C_b b) (parenthesized b)
      | Some c -> fail (Format.sprintf "Unexpected char: %c" c)
      | None -> fail (Format.sprintf "Unexpected: no input")

  let whitespace =
    many @@ choice [ string " "; string "\n"; string "\t" ] >>| ignore

  let c' = c <* many (whitespace <* char '.' <* whitespace)
end

module Pp = struct
  let rec a ppf = function
    | A_a a' -> Fmt.pf ppf "A_a (%a)" a a'
    | A_b b' -> Fmt.pf ppf "A_b (%a)" b b'
    | A_s s' -> Fmt.pf ppf "A_s %s" s'
  and b ppf = function
    | B_a a' -> Fmt.pf ppf "B_a (%a)" a a'
    | B_b b' -> Fmt.pf ppf "B_b (%a)" b b'
    | B_s s' -> Fmt.pf ppf "B_s %s" s'
  let c ppf = function
    | C_a a' -> Fmt.pf ppf "(%a)" a a'
    | C_b b' -> Fmt.pf ppf "(%a)" b b'
end

let pp = Pp.c
