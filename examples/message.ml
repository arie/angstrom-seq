(** Examples of strings, which can be parsed as messages, are
    'abcdef 3 .'
    'abcdefghijkl 5.4 .'
*)

type message = Message_int of string * int | Message_float of string * float

type t = message

module Parser = struct
  open Angstrom

  let take_until s = take_while (fun c -> Char.equal s c |> not)

  let integer =
    take_while1 (function '0' .. '9' -> true | _ -> false) >>| int_of_string

  let small_float = lift2
      (fun a b -> Float.of_string @@ (string_of_int a) ^ "." ^ (string_of_int b))
      integer
      (char '.' *> integer)

  let message_int = lift2
      (fun s i -> Message_int (s, i))
      (take_until ' ' <* char ' ')
      (integer <* char ' ' <* char '.')

  let message_float = lift2
      (fun s f -> Message_float (s, f))
      (take_until ' ')
      (char ' ' *> small_float <* char ' ' <* char '.')

  let message = (message_int <|> message_float) <* char '\n'
end

let pp ppf = function
  | Message_int (s, i) -> Fmt.pf ppf "%s %d" s i
  | Message_float (s, f) -> Fmt.pf ppf "%s %f" s f


